package com.qingcheng.service.impl;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qingcheng.dao.OrderConfigMapper;
import com.qingcheng.dao.OrderItemMapper;
import com.qingcheng.dao.OrderLogMapper;
import com.qingcheng.dao.OrderMapper;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.order.*;
import com.qingcheng.service.goods.SkuService;
import com.qingcheng.service.order.CartService;
import com.qingcheng.service.order.OrderService;
import com.qingcheng.util.IdWorker;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service(interfaceClass = OrderService.class )
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private OrderLogMapper orderLogMapper;

    @Autowired
    private OrderConfigMapper orderConfigMapper;
    /**
     * 返回全部记录
     * @return
     */
    public List<Order> findAll() {
        return orderMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Order> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Order> orders = (Page<Order>) orderMapper.selectAll();
        return new PageResult<Order>(orders.getTotal(),orders.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Order> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return orderMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Order> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Order> orders = (Page<Order>) orderMapper.selectByExample(example);
        return new PageResult<Order>(orders.getTotal(),orders.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Order findById(String id) {
        return orderMapper.selectByPrimaryKey(id);
    }


    @Autowired
    private CartService cartService;

    @Reference
    private SkuService skuService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 新增
     * @param order
     */
    public Map<String,Object> add(Order order) {
        //1,.获取选中的购物车
        List<Map<String, Object>> orderItemList = cartService.findNewOrderItemList(order.getUsername());
        List<OrderItem> orderItems = orderItemList.stream().filter(cart -> (boolean) cart.get("checked"))
                .map(cart -> (OrderItem) cart.get("item")).collect(Collectors.toList());

        //2.扣减库存
        if (!skuService.deductionStock(orderItems)){
            throw new RuntimeException("库存不足");
        }

        try {
            //3.保存订单主表
            order.setId(idWorker.nextId()+"");
            //合计数计算
            IntStream numStream = orderItems.stream().mapToInt(OrderItem::getNum);
            IntStream moneyStream = orderItems.stream().mapToInt(OrderItem::getMoney);

            int totalNum = numStream.sum();//总数量
            int totalMoney = moneyStream.sum();//订单总金额
            int preMoney = cartService.preferential(order.getUsername());//计算优惠金额

            order.setTotalNum(totalNum);//总数量
            order.setTotalMoney(totalMoney);//总金额
            order.setPreMoney(preMoney);//优惠金额
            order.setPayMoney(totalMoney-preMoney);//支付金额
            order.setCreateTime(new Date());//订单创建时间
            order.setOrderStatus("0");//订单状态
            order.setPayStatus("0");//支付状态:未支付
            order.setConsignStatus("0");//发货状态:未发货

            orderMapper.insert(order);
            //4.保存订单明细表
            //计算打折比例
            double proportion = (double)order.getPayMoney() / totalMoney;
            for (OrderItem orderItem : orderItems) {
                orderItem.setOrderId(order.getId());//订单主表id
                orderItem.setId(idWorker.nextId()+"");
                orderItem.setPayMoney( (int)(orderItem.getMoney()*proportion) );//支付金额

                orderItemMapper.insert(orderItem);
            }

            //int x = 1/0;
        } catch (Exception e) {
            e.printStackTrace();
            //发送回滚消息
            rabbitTemplate.convertAndSend("","queue.skuback", JSON.toJSONString(orderItems));
            throw new RuntimeException("创建订单失败");
        }
        //5.清除购物车
        cartService.deleteCheckedCart(order.getUsername());

        //返回订单号和支付金额
        Map<String,Object> map = new HashMap<>();
        map.put("ordersn",order.getId());//订单号
        map.put("money",order.getPayMoney());
        return map;
    }

    /**
     * 修改
     * @param order
     */
    public void update(Order order) {
        orderMapper.updateByPrimaryKeySelective(order);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        orderMapper.deleteByPrimaryKey(id);
    }

    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @Override
    public Orders findOrdersById(String id) {
        Orders orders = new Orders();
        //封装order
        Order order = orderMapper.selectByPrimaryKey(id);
        orders.setOrder(order);
        //封装orderitem
        Example example = new Example(OrderItem.class);
        Example.Criteria criteria = example.createCriteria();
        //根据它们关联的order_id查
        criteria.andEqualTo("orderId",order.getId());

        List<OrderItem> orderItemList = orderItemMapper.selectByExample(example);

        orders.setOrderItemList(orderItemList);

        return orders;
    }

    /**
     * 批量发货
     * @param orderList
     */
    @Override
    public void batchSend(List<Order> orderList) {
        //判断物流公司和运单号是否为空
        for (Order order : orderList) {
            if (order.getShippingName() == null || order.getShippingCode() == null){
                throw new RuntimeException("请选择快递公司和填写快递单号");
            }
        }
        for (Order order : orderList) {
            order.setOrderStatus("2");//订单状态  已发货
            order.setConsignStatus("1");//发货状态  已发货
            order.setConsignTime(new Date());//发货时间

            orderMapper.updateByPrimaryKeySelective(order);

            //记录订单日志
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId()+"");
            orderLog.setOperater("system");
            orderLog.setOperateTime(new Date());
            orderLog.setOrderId(order.getId());
            orderLog.setOrderStatus("1");
            orderLog.setPayStatus("1");
            orderLogMapper.insertSelective(orderLog);
        }
    }

    /**
     * 订单超时处理
     *
     */
    @Override
    public void orderTimeOutLogic() {
        OrderConfig orderConfig = orderConfigMapper.selectByPrimaryKey(1);
        Integer orderTimeout = orderConfig.getOrderTimeout();//获取超时时间
        //获取超时的时间点
        LocalDateTime localDateTime = LocalDateTime.now().minusMinutes(orderTimeout);

        //设置查询条件
        Example example = new Example(Order.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLessThan("createTime",localDateTime);//创建时间小于超时时间
        criteria.andEqualTo("orderStatus","0");//未付款
        criteria.andEqualTo("isDelete","0");//未删除
        //查询超时订单
        List<Order> orders = orderMapper.selectByExample(example);

        for (Order order : orders) {
            //记录日志
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId()+"");
            orderLog.setOperater("system");
            orderLog.setOperateTime(new Date());
            orderLog.setOrderId(order.getId());
            orderLog.setOrderStatus("4");
            orderLog.setPayStatus(order.getPayStatus());
            orderLog.setConsignStatus(order.getConsignStatus());
            orderLog.setRemarks("订单超时,系统自动关闭");
            orderLogMapper.insert(orderLog);

            //改变订单状态
            order.setOrderStatus("4");
            order.setCloseTime(new Date());
            orderMapper.updateByPrimaryKeySelective(order);

        }

    }

    /**
     * 订单合并
     * @param orderIdMaster
     * @param orderIdFollow
     */
    @Override
    @Transactional
    public void merge(String orderIdMaster, String orderIdFollow) {
        Order orderMaster = orderMapper.selectByPrimaryKey(orderIdMaster);
        Order orderFollow = orderMapper.selectByPrimaryKey(orderIdFollow);

        Order order = new Order();
        order.setId(orderMaster.getId());
        order.setTotalNum(orderMaster.getTotalNum()+orderFollow.getTotalNum());
        order.setTotalMoney(orderMaster.getTotalMoney()+orderFollow.getTotalMoney());
        order.setPreMoney(orderMaster.getPreMoney()+orderMaster.getPreMoney());
        order.setPayMoney(orderMaster.getPayMoney()+orderFollow.getPayMoney());
        order.setUpdateTime(new Date());

        orderMapper.updateByPrimaryKeySelective(order);//合并到主表
        //“从订单”的订单明细也归属于主订单。
        Example example = new Example(OrderItem.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("orderId",orderIdFollow);
        List<OrderItem> orderItemList = orderItemMapper.selectByExample(example);
        //把从表明细的orderID改为主表的ID
        for (OrderItem orderItem : orderItemList) {
            orderItem.setOrderId(orderIdMaster);
            orderItemMapper.updateByPrimaryKeySelective(orderItem);
        }
        //将从表逻辑删除
        orderFollow.setIsDelete("1");
        orderMapper.updateByPrimaryKeySelective(orderFollow);
        //记录日志
        OrderLog orderLog = new OrderLog();
        orderLog.setId(idWorker.nextId()+"");
        orderLog.setOperater("system");
        orderLog.setOperateTime(new Date());
        orderLog.setOrderId(orderIdMaster);
        orderLogMapper.insertSelective(orderLog);
    }

    /**
     * 订单拆分
     * @param mapList
     */
    @Override
    @Transactional
    public void split(List<Map<String, String>> mapList) {

        //统计个数
        Integer total_num = 0;//商品数量
        Integer total_money = 0;//金额合计
        //Integer pre_money = 0;//优惠金额
        Integer pay_money = 0;//实付金额

        Integer total_num2 = 0;//商品数量
        Integer total_money2 = 0;//金额合计
        //Integer pre_money2 = 0;//优惠金额
        Integer pay_money2 = 0;//实付金额

        //固定新订单明细的orderID和新订单的ID相同
        String newOrderId = idWorker.nextId()+"";

        for (Map<String, String> map : mapList) {
            String id = map.get("id");
            //查询明细订单
            OrderItem orderItem1 = orderItemMapper.selectByPrimaryKey(id);
            if (orderItem1.getNum() < Integer.parseInt(map.get("num") ) || Integer.parseInt(map.get("num")) < 0){
                throw new RuntimeException("输入的拆分数量有误,请重新输入!");
            }
            //统计主订单原来个数
           /* total_num += orderItem1.getNum();
            total_money += orderItem1.getMoney();
            //pre_money += orderItem1.getMoney()-orderItem1.getPayMoney();//表有问题
            pay_money += orderItem1.getPayMoney();*/

            //克隆
            OrderItem neworderItem = new OrderItem();
            BeanUtils.copyProperties(orderItem1,neworderItem);


            Integer spileNum = Integer.parseInt(map.get("num"));
            //拆分后旧订单明细的个数


            Integer oldNum =  orderItem1.getNum() - spileNum;
            Integer oldMoney = orderItem1.getMoney() - (spileNum * orderItem1.getPrice());
            Integer oldpay_money = oldMoney;
            //设置old
            orderItem1.setNum(oldNum);
            orderItem1.setMoney(oldMoney);
            orderItem1.setPayMoney(oldpay_money);

            //拆分后新订单明细
            String newId = idWorker.nextId()+"";
            Integer newNum = spileNum;
            Integer newMoney = spileNum * orderItem1.getPrice();
            Integer newpay_money = newMoney;

            //设置new
            neworderItem.setId(newId);
            neworderItem.setNum(newNum);
            neworderItem.setMoney(newMoney);
            neworderItem.setPayMoney(newpay_money);
            neworderItem.setOrderId(newOrderId);

            //统计主订单剩下个数
            total_num += oldNum;
            total_money += oldMoney;
            //pre_money += orderItem1.getMoney()-orderItem1.getPayMoney();//表有问题
            pay_money += oldpay_money;
            //统计从订单剩下个数
            total_num2 += newNum;
            total_money2 += newMoney;
            //pre_money2 += orderItem1.getMoney()-orderItem1.getPayMoney();//表有问题
            pay_money2 += newpay_money;

            //更新old
            orderItemMapper.updateByPrimaryKeySelective(orderItem1);
            System.out.println(orderItem1);
            //更新new
            orderItemMapper.insertSelective(neworderItem);
            System.out.println(neworderItem);
            //记录日志old
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId()+"");
            orderLog.setOperater("system");
            orderLog.setOperateTime(new Date());
            orderLog.setOrderId(orderItem1.getOrderId());
            orderLog.setRemarks("olditem更新");
            orderLogMapper.insertSelective(orderLog);
            //记录日志new
            OrderLog orderLog2 = new OrderLog();
            orderLog2.setId(idWorker.nextId()+"");
            orderLog2.setOperater("system");
            orderLog2.setOperateTime(new Date());
            orderLog2.setOrderId(neworderItem.getOrderId());
            orderLog2.setRemarks("newitem更新");
            orderLogMapper.insertSelective(orderLog2);

        }



        String id = mapList.get(0).get("id");
        //查询明细订单
        OrderItem orderItem1 = orderItemMapper.selectByPrimaryKey(id);

        //查询主订单
        Order orderMaster = orderMapper.selectByPrimaryKey(orderItem1.getOrderId());

        Order orderFollow = new Order();
        //克隆
        BeanUtils.copyProperties(orderMaster,orderFollow);
        //设置拆分后的主订单
        orderMaster.setTotalNum(total_num);
        orderMaster.setTotalMoney(total_money);
        orderMaster.setPayMoney(pay_money);
        orderMaster.setUpdateTime(new Date());
        //设置拆分后的  从 订单
        orderFollow.setId(newOrderId);
        orderFollow.setTotalNum(total_num2);
        orderFollow.setTotalMoney(total_money2);
        orderFollow.setPayMoney(pay_money2);
        orderFollow.setUpdateTime(new Date());

        //更新
        orderMapper.updateByPrimaryKeySelective(orderMaster);

        orderMapper.insertSelective(orderFollow);

        //记录日志master
        OrderLog orderLog = new OrderLog();
        orderLog.setId(idWorker.nextId()+"");
        orderLog.setOperater("system");
        orderLog.setOperateTime(new Date());
        orderLog.setOrderId(orderMaster.getId());
        orderLog.setRemarks("master更新");
        orderLogMapper.insertSelective(orderLog);
        //记录日志follow
        OrderLog orderLog2 = new OrderLog();
        orderLog2.setId(idWorker.nextId()+"");
        orderLog2.setOperater("system");
        orderLog2.setOperateTime(new Date());
        orderLog2.setOrderId(orderFollow.getId());
        orderLog2.setRemarks("follow更新");
        orderLogMapper.insertSelective(orderLog2);
    }

    @Override
    public void updatePayStatus(String orderId, String transactionId) {
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if (order!=null && "0".equals(order.getPayStatus())){
            //修改订单状态等信息
            order.setPayStatus("1");//支付状态
            order.setOrderStatus("1");//订单状态
            order.setUpdateTime(new Date());//修改日期
            order.setPayTime(new Date());//支付日期
            order.setTransactionId(transactionId);//交易流水号
            orderMapper.updateByPrimaryKeySelective(order);//修改

            //记录订单日志
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId()+"");
            orderLog.setOperater("system");//系统
            orderLog.setOperateTime(new Date());//操作时间
            orderLog.setOrderStatus("1");//订单状态
            orderLog.setPayStatus("1");//支付状态
            orderLog.setRemarks("支付流水号:"+transactionId);//备注
            orderLog.setOrderId(orderId);
            orderLogMapper.insert(orderLog);
        }
    }


    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Order.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){

            // 根据 id 数组查询 查询
            if(searchMap.get("ids")!=null ){
                JSONArray jsonArray1 = (JSONArray) searchMap.get("ids");
                List<String> strings = jsonArray1.toJavaList(String.class);
                criteria.andIn("id",strings);
                System.out.println(strings);
            }
            // 订单id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 支付类型，1、在线支付、0 货到付款
            if(searchMap.get("payType")!=null && !"".equals(searchMap.get("payType"))){
                criteria.andLike("payType","%"+searchMap.get("payType")+"%");
            }
            // 物流名称
            if(searchMap.get("shippingName")!=null && !"".equals(searchMap.get("shippingName"))){
                criteria.andLike("shippingName","%"+searchMap.get("shippingName")+"%");
            }
            // 物流单号
            if(searchMap.get("shippingCode")!=null && !"".equals(searchMap.get("shippingCode"))){
                criteria.andLike("shippingCode","%"+searchMap.get("shippingCode")+"%");
            }
            // 用户名称
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andLike("username","%"+searchMap.get("username")+"%");
            }
            // 买家留言
            if(searchMap.get("buyerMessage")!=null && !"".equals(searchMap.get("buyerMessage"))){
                criteria.andLike("buyerMessage","%"+searchMap.get("buyerMessage")+"%");
            }
            // 是否评价
            if(searchMap.get("buyerRate")!=null && !"".equals(searchMap.get("buyerRate"))){
                criteria.andLike("buyerRate","%"+searchMap.get("buyerRate")+"%");
            }
            // 收货人
            if(searchMap.get("receiverContact")!=null && !"".equals(searchMap.get("receiverContact"))){
                criteria.andLike("receiverContact","%"+searchMap.get("receiverContact")+"%");
            }
            // 收货人手机
            if(searchMap.get("receiverMobile")!=null && !"".equals(searchMap.get("receiverMobile"))){
                criteria.andLike("receiverMobile","%"+searchMap.get("receiverMobile")+"%");
            }
            // 收货人地址
            if(searchMap.get("receiverAddress")!=null && !"".equals(searchMap.get("receiverAddress"))){
                criteria.andLike("receiverAddress","%"+searchMap.get("receiverAddress")+"%");
            }
            // 订单来源：1:web，2：app，3：微信公众号，4：微信小程序  5 H5手机页面
            if(searchMap.get("sourceType")!=null && !"".equals(searchMap.get("sourceType"))){
                criteria.andLike("sourceType","%"+searchMap.get("sourceType")+"%");
            }
            // 交易流水号
            if(searchMap.get("transactionId")!=null && !"".equals(searchMap.get("transactionId"))){
                criteria.andLike("transactionId","%"+searchMap.get("transactionId")+"%");
            }
            // 订单状态
            if(searchMap.get("orderStatus")!=null && !"".equals(searchMap.get("orderStatus"))){
                criteria.andLike("orderStatus","%"+searchMap.get("orderStatus")+"%");
            }
            // 支付状态
            if(searchMap.get("payStatus")!=null && !"".equals(searchMap.get("payStatus"))){
                criteria.andLike("payStatus","%"+searchMap.get("payStatus")+"%");
            }
            // 发货状态
            if(searchMap.get("consignStatus")!=null && !"".equals(searchMap.get("consignStatus"))){
                criteria.andLike("consignStatus","%"+searchMap.get("consignStatus")+"%");
            }
            // 是否删除
            if(searchMap.get("isDelete")!=null && !"".equals(searchMap.get("isDelete"))){
                criteria.andLike("isDelete","%"+searchMap.get("isDelete")+"%");
            }

            // 数量合计
            if(searchMap.get("totalNum")!=null ){
                criteria.andEqualTo("totalNum",searchMap.get("totalNum"));
            }
            // 金额合计
            if(searchMap.get("totalMoney")!=null ){
                criteria.andEqualTo("totalMoney",searchMap.get("totalMoney"));
            }
            // 优惠金额
            if(searchMap.get("preMoney")!=null ){
                criteria.andEqualTo("preMoney",searchMap.get("preMoney"));
            }
            // 邮费
            if(searchMap.get("postFee")!=null ){
                criteria.andEqualTo("postFee",searchMap.get("postFee"));
            }
            // 实付金额
            if(searchMap.get("payMoney")!=null ){
                criteria.andEqualTo("payMoney",searchMap.get("payMoney"));
            }

        }
        return example;
    }

}
