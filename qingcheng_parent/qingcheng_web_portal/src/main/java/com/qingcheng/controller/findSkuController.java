package com.qingcheng.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.Result;
import com.qingcheng.service.goods.SkuService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/find")
public class findSkuController {

    @Reference(timeout = 900000000)
    private SkuService skuService;

    @GetMapping("/findSku")
    public Result findSku(){
        skuService.findSkuElasticsearch();
        return new Result();
    }
}
