package com.qingcheng.controller.system;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.qingcheng.entity.PageResult;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.system.Admin;
import com.qingcheng.service.system.AdminService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Reference
    private AdminService adminService;

    @GetMapping("/findAll")
    public List<Admin> findAll(){
        return adminService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Admin> findPage(int page, int size){
        return adminService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Admin> findList(@RequestBody Map<String,Object> searchMap){
        return adminService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Admin> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  adminService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Admin findById(Integer id){
        return adminService.findById(id);
    }

    @GetMapping("/findByName")
    public Admin findByName(){
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        return adminService.findByName(name);
    }


    /*@PostMapping("/add")
    public Result add(@RequestBody Admin admin){
        adminService.add(admin);
        return new Result();
    }*/

    /*@PostMapping("/update")
    public Result update(@RequestBody Admin admin){
        adminService.update(admin);
        return new Result();
    }*/

    @PostMapping("/updatePassword")
    public Result updatePassword( @RequestBody Map<String,String> passwordMap){
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        adminService.updatePassword(name,passwordMap);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(Integer id){
        adminService.delete(id);
        return new Result();
    }

    @PostMapping("/add")
    public Result add(@RequestBody Map<String,Object> map){

        //验证两次输入的密码是否一致
        if (!map.get("pass").equals(map.get("checkPass"))){
            throw new RuntimeException("两次密码不一致");
        }
        if (map.get("pass") == null || "".equals(map.get("pass"))){
            throw new RuntimeException("密码不能为空");
        }


        List<Integer> listRoles = ((JSONArray)(map.get("selectedRoles"))).toJavaList(Integer.class);
        map.put("selectedRoles",listRoles);
        adminService.add(map);

        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Map<String,Object> map,String id){

        System.out.println(map.get("pass"));
        //验证两次输入的密码是否一致
        if (map.get("pass") != null) {
            if (!map.get("pass").equals(map.get("checkPass"))) {
                throw new RuntimeException("两次密码不一致");
            }
        }
        //修改,如果密码为空,就是用原来的密码
        /*if (map.get("pass") == null || "".equals(map.get("pass"))){
            throw new RuntimeException("密码不能为空");
        }*/
        List<Integer> listRoles = ((JSONArray)(map.get("selectedRoles"))).toJavaList(Integer.class);
        map.put("selectedRoles",listRoles);
        adminService.update(map,Integer.parseInt(id));

        return new Result();
    }

}
