package com.qingcheng.controller.goods;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.service.goods.StockBackService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class skuTask {

    @Reference
    private StockBackService stockBackService;


    //@Scheduled(cron = "0 0 0/1 * * ?")
    @Scheduled(cron = "0 * * * * ?")
    public void skuStockBack(){
        System.out.println("库存回滚开始");
        stockBackService.doBack();
        System.out.println("库存回滚结束");
    }

}
