package com.qingcheng.controller.order;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.PageResult;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.order.Order;
import com.qingcheng.pojo.order.Orders;
import com.qingcheng.service.order.OrderService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Reference
    private OrderService orderService;

    @GetMapping("/findAll")
    public List<Order> findAll(){
        return orderService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Order> findPage(int page, int size){
        return orderService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Order> findList(@RequestBody Map<String,Object> searchMap){
        return orderService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Order> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  orderService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Order findById(String id){
        return orderService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Order order){
        orderService.add(order);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Order order){
        orderService.update(order);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        orderService.delete(id);
        return new Result();
    }

    //订单列表和详情
    @GetMapping("/findOrdersById")
    public Orders findOrdersById(String id){
        return orderService.findOrdersById(id);
    }

    //批量发货
    @PostMapping("/batchSend")
    public Result batchSend(@RequestBody List<Order> orderList){
        orderService.batchSend(orderList);
        return new Result();
    }

    //订单合并
    @GetMapping("/merge")
    public Result merge(String orderIdMaster, String orderIdFollow){
        orderService.merge(orderIdMaster,orderIdFollow);
        return new Result();
    }

    //订单拆分
    @PostMapping("/split")
    public Result split( @RequestBody List<Map<String, String>> mapList){
        orderService.split(mapList);
        return new Result();
    }

}
