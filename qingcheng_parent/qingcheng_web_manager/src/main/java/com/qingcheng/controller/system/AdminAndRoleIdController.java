package com.qingcheng.controller.system;


import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.pojo.system.AdminAndRoleId;
import com.qingcheng.service.system.AdminAndRoleIdService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/adminAndRoleId")
public class AdminAndRoleIdController {

    @Reference
    private AdminAndRoleIdService adminAndRoleIdService;

    @RequestMapping("/findAdminAndRoleIdById")
    public AdminAndRoleId findAdminAndRoleIdById(String id){
        return adminAndRoleIdService.findAdminAndRoleIdById(Integer.parseInt(id));
    }
}
