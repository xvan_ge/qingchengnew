package com.qingcheng.pojo.system;

import java.io.Serializable;
import java.util.List;

/**
 * 前后端约定要提交的数据格式，包括“角色id”和“权限id列表”。
 */
public class RoleIdAndResourceIdList implements Serializable {

    private Integer roleId;

    private List<Integer> resourceIdList;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public List<Integer> getResourceIdList() {
        return resourceIdList;
    }

    public void setResourceIdList(List<Integer> resourceIdList) {
        this.resourceIdList = resourceIdList;
    }
}
