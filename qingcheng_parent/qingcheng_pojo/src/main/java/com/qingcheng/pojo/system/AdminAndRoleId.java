package com.qingcheng.pojo.system;

import java.io.Serializable;
import java.util.List;

public class AdminAndRoleId implements Serializable {

    private Admin admin;
    private List<Integer> listId;

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public List<Integer> getListId() {
        return listId;
    }

    public void setListId(List<Integer> listId) {
        this.listId = listId;
    }
}
