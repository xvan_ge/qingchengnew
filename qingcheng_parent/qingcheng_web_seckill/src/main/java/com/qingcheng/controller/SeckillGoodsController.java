package com.qingcheng.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.pojo.seckill.SeckillGoods;
import com.qingcheng.service.seckill.SeckillGoodsService;
import com.qingcheng.util.DateUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping( value = "/seckill/goods")
public class SeckillGoodsController {

    @Reference(timeout = 300000)
    private SeckillGoodsService seckillGoodsService;

    /**
     * 根据商品id查询商品详情
     * @param time  商品秒杀时区
     * @param id  商品Id
     * @return
     */
    @GetMapping(value = "/one")
    public SeckillGoods one(String time,Long id){
        return seckillGoodsService.one(time,id);
    }


    /**
     * 加载对应时区的秒杀商品
     * @param time  2019052012
     * @return
     */
    @GetMapping(value = "/list")
    public List<SeckillGoods> list(String time){
        List<SeckillGoods> list = seckillGoodsService.list(time);
        return list;
    }

    /**
     * 加载所有时间菜单
     * @return
     */
    @RequestMapping(value = "/menus")
    public List<Date> loadMenus(){
        return DateUtil.getDateMenus();
    }


}
