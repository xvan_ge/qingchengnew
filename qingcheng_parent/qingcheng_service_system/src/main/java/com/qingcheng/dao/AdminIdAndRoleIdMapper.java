package com.qingcheng.dao;

import com.qingcheng.pojo.system.AdminIdAndRoleId;
import tk.mybatis.mapper.common.Mapper;

public interface AdminIdAndRoleIdMapper extends Mapper<AdminIdAndRoleId> {
}
