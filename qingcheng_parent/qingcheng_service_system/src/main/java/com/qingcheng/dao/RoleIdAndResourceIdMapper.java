package com.qingcheng.dao;

import com.qingcheng.pojo.system.RoleIdAndResourceId;
import tk.mybatis.mapper.common.Mapper;

public interface RoleIdAndResourceIdMapper extends Mapper<RoleIdAndResourceId> {
}
