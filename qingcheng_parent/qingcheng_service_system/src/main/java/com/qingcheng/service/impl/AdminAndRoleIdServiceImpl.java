package com.qingcheng.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.qingcheng.dao.AdminIdAndRoleIdMapper;
import com.qingcheng.dao.AdminMapper;
import com.qingcheng.pojo.system.Admin;
import com.qingcheng.pojo.system.AdminAndRoleId;
import com.qingcheng.pojo.system.AdminIdAndRoleId;
import com.qingcheng.service.system.AdminAndRoleIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass = AdminAndRoleIdService.class )
public class AdminAndRoleIdServiceImpl implements AdminAndRoleIdService {

    @Autowired
    private AdminMapper adminMapper;
    
    @Autowired
    private AdminIdAndRoleIdMapper adminIdAndRoleIdMapper;

    @Override
    @Transactional
    public AdminAndRoleId findAdminAndRoleIdById(Integer id) {
        AdminAndRoleId adminAndRoleId = new AdminAndRoleId();

        Admin admin = adminMapper.selectByPrimaryKey(id);
        
        Example example = new Example(AdminIdAndRoleId.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("adminId",admin.getId());
        List<AdminIdAndRoleId> adminIdAndRoleIds = adminIdAndRoleIdMapper.selectByExample(example);
        List<Integer> roleIds = new ArrayList<Integer>();
        for (AdminIdAndRoleId adminIdAndRoleId : adminIdAndRoleIds) {

            roleIds.add(adminIdAndRoleId.getRoleId());
        }

        //赋值
        adminAndRoleId.setAdmin(admin);
        adminAndRoleId.setListId(roleIds);


        System.out.println(adminAndRoleId);
        return adminAndRoleId;
    }
}
