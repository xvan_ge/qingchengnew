package com.qingcheng.service.order;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.order.Order;
import com.qingcheng.pojo.order.Orders;

import java.util.*;

/**
 * order业务逻辑层
 */
public interface OrderService {


    public List<Order> findAll();


    public PageResult<Order> findPage(int page, int size);


    public List<Order> findList(Map<String,Object> searchMap);


    public PageResult<Order> findPage(Map<String,Object> searchMap,int page, int size);


    public Order findById(String id);

    //新改的返回值
    public Map<String,Object> add(Order order);


    public void update(Order order);


    public void delete(String id);

    public Orders findOrdersById(String id);

    /**
     * 批量发货
     * @param orderList
     */
    public void batchSend(List<Order> orderList);

    /**
     * 订单超时处理
     */
    public void orderTimeOutLogic();

    //订单合并
    public void merge(String orderIdMaster,String orderIdFollow);

    //订单拆分
    public void split(List<Map<String,String>> mapList);

    /**
     * 修改订单状态
     * @param orderId
     * @param transactionId
     */
    public void updatePayStatus(String orderId,String transactionId);
}
