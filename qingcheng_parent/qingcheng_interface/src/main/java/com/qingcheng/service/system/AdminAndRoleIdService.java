package com.qingcheng.service.system;

import com.qingcheng.pojo.system.AdminAndRoleId;

public interface AdminAndRoleIdService {

    //编辑和添加成员的回显
    public AdminAndRoleId findAdminAndRoleIdById(Integer id);
}
