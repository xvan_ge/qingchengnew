package com.qingcheng.service.order;

import java.util.Map;

public interface WxPayService {

    /**
     * 生成微信支付二维码(统一下单)
     * @param orderId 订单号
     * @param money 金额(分)
     * @param notifyUrl 回调地址
     * @param attach 附加数据
     * @return
     */
    public Map createNative(String orderId,Integer money,String notifyUrl,String...attach);

    /**
     * 微信支付回调
     * @param xml
     */
    public void nitifyLogic(String xml);

    /***
     *  关闭支付订单
     *  @param orderId
     *  @return
     */
    public Map<String, String> closePay(Long orderId) throws Exception;


}
