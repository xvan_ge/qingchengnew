package com.qingcheng.service.system;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.system.Admin;

import java.util.*;

/**
 * admin业务逻辑层
 */
public interface AdminService {


    public List<Admin> findAll();


    public PageResult<Admin> findPage(int page, int size);


    public List<Admin> findList(Map<String,Object> searchMap);


    public PageResult<Admin> findPage(Map<String,Object> searchMap,int page, int size);


    public Admin findById(Integer id);

    /*public void add(Admin admin);*/


    /*public void update(Admin admin);*/


    public void delete(Integer id);

    Admin findByName(String name);

    void updatePassword(String name, Map<String, String> passwordMap);

    public void add(Map<String,Object> map);

    public void update(Map<String,Object> map,Integer id);
}
