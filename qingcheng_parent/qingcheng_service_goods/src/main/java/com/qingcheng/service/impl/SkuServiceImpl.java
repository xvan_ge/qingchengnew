package com.qingcheng.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qingcheng.dao.SkuMapper;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.goods.Sku;
import com.qingcheng.pojo.order.OrderItem;
import com.qingcheng.service.goods.SkuService;
import com.qingcheng.util.CacheKey;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = SkuService.class)
public class SkuServiceImpl implements SkuService {

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 返回全部记录
     * @return
     */
    public List<Sku> findAll() {
        return skuMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Sku> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Sku> skus = (Page<Sku>) skuMapper.selectAll();
        return new PageResult<Sku>(skus.getTotal(),skus.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Sku> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return skuMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Sku> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Sku> skus = (Page<Sku>) skuMapper.selectByExample(example);
        return new PageResult<Sku>(skus.getTotal(),skus.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Sku findById(String id) {
        return skuMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param sku
     */
    public void add(Sku sku) {
        skuMapper.insert(sku);
    }

    /**
     * 修改
     * @param sku
     */
    public void update(Sku sku) {
        skuMapper.updateByPrimaryKeySelective(sku);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        skuMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void saveAllPriceToRedis() {

        if (!redisTemplate.hasKey(CacheKey.SKU_PRICE)){
            System.out.println("商品缓存预热");
            //查询所有商品价格
            List<Sku> skuList = skuMapper.selectAll();
            for (Sku sku : skuList) {
                if ("1".equals(sku.getStatus())){
                    //放入缓存
                    redisTemplate.boundHashOps(CacheKey.SKU_PRICE).put(sku.getId(),sku.getPrice());
                }
            }
        }else {
            System.out.println("已存在价格,跳过缓存预热");

        }
    }

    @Override
    public Integer findPrice(String id) {
        return (Integer) redisTemplate.boundHashOps(CacheKey.SKU_PRICE).get(id);
    }

    @Override
    public void savePriceToRedisById(String id, Integer price) {
        redisTemplate.boundHashOps(CacheKey.SKU_PRICE).put(id,price);
    }

    @Override
    public void deletePriceFromRedis(String id) {
        redisTemplate.boundHashOps(CacheKey.SKU_PRICE).delete(id);
    }


    @Override
    public void findSkuElasticsearch() {

        //1.连接rest接口
        HttpHost http = new HttpHost("127.0.0.1",9200,"http");
        RestClientBuilder builder = RestClient.builder(http);//rest构建器
        builder.setMaxRetryTimeoutMillis(900000000);
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(builder);//高级客户端对象

        //2.封装请求对象
        BulkRequest bulkRequest = new BulkRequest();//批量操作
        List<Sku> skuList = findAll();
        for (Sku sku : skuList) {

            IndexRequest indexRequest = new IndexRequest("sku","doc",sku.getId());
            Map map = new HashMap();
            map.put("name",sku.getName()); // sku名称
            map.put("sn",sku.getSn()); // 商品条码
            map.put("price",sku.getPrice()); // 价格
            map.put("num",sku.getNum()); // 库存数量
            map.put("alertNum",sku.getAlertNum()); // 库存预警数量
            map.put("image",sku.getImage()); // 商品图片
            map.put("images",sku.getImages()); // 商品图片列表
            map.put("weight",sku.getWeight()); // 重量
            map.put("createTime",sku.getCreateTime()); // 创建时间
            map.put("updateTime",sku.getUpdateTime()); // 更新时间
            map.put("spuId",sku.getSpuId()); // SPUID
            map.put("categoryId",sku.getCategoryId()); // 类目ID
            map.put("categoryName",sku.getCategoryName());// 类目名称
            map.put("brandName",sku.getBrandName()); // 品牌名称
            map.put("spec",(Map)JSONObject.parse(sku.getSpec())); //  规格

            /*String spec = sku.getSpec();
            Map parse = (Map) JSONObject.parse(spec);*/


            /* Map spec = new HashMap();
            Map<String,String> mapSpec = (Map)JSONObject.parseObject(sku.getSpec());
            for (String key : mapSpec.keySet()) {
                spec.put(key,mapSpec.get(key));
            }
            map.put("spec",spec);*/

            map.put("saleNum",sku.getSaleNum()); // 销量
            map.put("commentNum",sku.getCommentNum()); // 评论数
            map.put("status",sku.getStatus()); // 商品状态

            indexRequest.source(map);

            bulkRequest.add(indexRequest);

        }
        //3.获取响应结果
        // IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);

        try {
            BulkResponse  bulkResponse = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            int status = bulkResponse.status().getStatus();
            System.out.println(status);
            restHighLevelClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    @Transactional
    public boolean deductionStock(List<OrderItem> orderItemList) {

        //检查是否可以扣减库存
        boolean isDeduction=true;//是否可以扣减
        for (OrderItem orderItem : orderItemList) {
            Sku sku = findById(orderItem.getSkuId());
            if (sku == null){
                isDeduction = false;
                break;
            }
            if (!"1".equals(sku.getStatus())){
                isDeduction = false;
                break;
            }
            if (sku.getNum().intValue()<orderItem.getNum().intValue()){
                isDeduction = false;
                break;
            }
        }

        //执行扣减
        if (isDeduction){
            for (OrderItem orderItem : orderItemList) {
                skuMapper.deductionStock(orderItem.getSkuId(),orderItem.getNum());//扣减库存

                skuMapper.addSaleNum(orderItem.getSkuId(),orderItem.getNum());//增加销量
            }
        }
        return isDeduction;
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Sku.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 商品id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 商品条码
            if(searchMap.get("sn")!=null && !"".equals(searchMap.get("sn"))){
                criteria.andLike("sn","%"+searchMap.get("sn")+"%");
            }
            // SKU名称
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 商品图片
            if(searchMap.get("image")!=null && !"".equals(searchMap.get("image"))){
                criteria.andLike("image","%"+searchMap.get("image")+"%");
            }
            // 商品图片列表
            if(searchMap.get("images")!=null && !"".equals(searchMap.get("images"))){
                criteria.andLike("images","%"+searchMap.get("images")+"%");
            }
            // SPUID
            if(searchMap.get("spuId")!=null && !"".equals(searchMap.get("spuId"))){
                criteria.andLike("spuId","%"+searchMap.get("spuId")+"%");
            }
            // 类目名称
            if(searchMap.get("categoryName")!=null && !"".equals(searchMap.get("categoryName"))){
                criteria.andLike("categoryName","%"+searchMap.get("categoryName")+"%");
            }
            // 品牌名称
            if(searchMap.get("brandName")!=null && !"".equals(searchMap.get("brandName"))){
                criteria.andLike("brandName","%"+searchMap.get("brandName")+"%");
            }
            // 规格
            if(searchMap.get("spec")!=null && !"".equals(searchMap.get("spec"))){
                criteria.andLike("spec","%"+searchMap.get("spec")+"%");
            }
            // 商品状态 1-正常，2-下架，3-删除
            if(searchMap.get("status")!=null && !"".equals(searchMap.get("status"))){
                criteria.andLike("status","%"+searchMap.get("status")+"%");
            }

            // 价格（分）
            if(searchMap.get("price")!=null ){
                criteria.andEqualTo("price",searchMap.get("price"));
            }
            // 库存数量
            if(searchMap.get("num")!=null ){
                criteria.andEqualTo("num",searchMap.get("num"));
            }
            // 库存预警数量
            if(searchMap.get("alertNum")!=null ){
                criteria.andEqualTo("alertNum",searchMap.get("alertNum"));
            }
            // 重量（克）
            if(searchMap.get("weight")!=null ){
                criteria.andEqualTo("weight",searchMap.get("weight"));
            }
            // 类目ID
            if(searchMap.get("categoryId")!=null ){
                criteria.andEqualTo("categoryId",searchMap.get("categoryId"));
            }
            // 销量
            if(searchMap.get("saleNum")!=null ){
                criteria.andEqualTo("saleNum",searchMap.get("saleNum"));
            }
            // 评论数
            if(searchMap.get("commentNum")!=null ){
                criteria.andEqualTo("commentNum",searchMap.get("commentNum"));
            }

        }
        return example;
    }

}
